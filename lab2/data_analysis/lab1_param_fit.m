clear
open_data;

e = 22;
sc_th = 0.6;
oc_th = 10e-3;

exp_v = data(e).v;
exp_i = data(e).i;
exp_t = data(e).t;
%% determine Isc
sc_i = [];
j = 1;
for i = 1:length(data(e).t)
    if (data(e).v(i) < sc_th)
        sc_i(j) = data(e).i(i);
        j = j+1;
    end
end

Isc(e) = mean(sc_i);

%% determine Voc
oc_v = [];
j = 1;
for i = 1:length(data(e).t)
    if (data(e).i(i) < oc_th)
        oc_v(j) = data(e).v(i);
        j = j+1;
    end
end

Voc(e) = mean(oc_v);

%% determine n and Is
% direct fit the model
figure
Id_fit2 = fit(exp_v,exp_i,'c-a*(exp(b*x)-1)', 'StartPoint',[Isc(e) 1e-6 8])
hold on
scatter(exp_v, exp_i);
plot(Id_fit2);
hold off
% discount the photoelectric current from the current data
Id = Isc(e) - exp_i; % diode current
figure
scatter(exp_v, Id);
Id_fit = fit(exp_v,Id,'exp1')
hold on
plot(Id_fit);
hold off

% try to find a straight line in a log log plot
figure
hold on
scatter(exp_v,log(Id), 'd')
j = 1;
for i = 1:length(exp_v)
    if exp_v(i) > 1.2
        v_fit(j) = exp_v(i);
        i_fit(j) = Id(i);
        j=j+1;
    end
end
fit_log = fit(v_fit',log(i_fit)','a*x+b', 'StartPoint',[log(50e-6) 8])
plot(fit_log)
coeffs = coeffvalues(fit_log);
Is = exp(coeffs(2))
n = 1/(coeffs(1)*3*0.026)
hold off

figure
hold on
scatter(exp_v, Id)
fit_exp = fit(v_fit', i_fit', 'a*exp(b*x)+c', 'StartPoint',[1e-6 8 0])
plot(fit_exp);
hold off