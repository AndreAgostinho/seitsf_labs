
open_data;
exp_labels = {['50 cm, 33 ' char(176) 'C'],...
    ['40 cm, 33 ' char(176) 'C'],...
    ['25 cm, 32 ' char(176) 'C'],...
    ['25 cm, 37.5 ' char(176) 'C']};
data_n = 21:24;
%plot all IU curves
figure
hold on
for i = data_n
    scatter(data(i).v,data(i).i * 1000)
end
hold off
title('IU Curves')
xlabel('U [V]') 
ylabel('I [mA]')
legend(exp_labels);

%plot all PU curves
figure
hold on
for i = data_n
    scatter(data(i).v, data(i).v.*data(i).i*1000)
end
hold off
title('PU Curves')
xlabel('U [V]') 
ylabel('P [mW]')
legend(exp_labels);

%plot all effciency-voltage curves
figure
hold on
for i = data_n
    scatter(data(i).v, data(i).v.*data(i).i./data(i).in_P*100)
end
hold off
title('Efficiency Curves')
xlabel('U [V]') 
ylabel('\eta [%]')
legend(exp_labels);
