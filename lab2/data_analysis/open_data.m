addpath('../data');

%% Put all file contents in a matrix
files = dir('../data/try*_exp*.csv');
conditions = readtable('../data/experiment_conditions.csv');
files_data = zeros(length(files), 1000, 4);
k=1;
data = struct();
for j = 1:4
    for i = 1:10
        s = sprintf('../data/try%d_exp%d.csv',j,i);
        a = fopen(s);
        if a ~= -1
            data_array = table2array(readtable(s));
            data(k).t = data_array(:, 1);
            data(k).v = data_array(:, 2);
            data(k).i = data_array(:, 4);
            data(k).in_P = conditions.inputPower_W_(k);
            k=k+1;
        end
    end
end



%% strucutre the data for easy use

distance_labels = {'50 cm', '40 cm', '25 cm'};
shading_labels = {'No shading', 'Half cell shaded', 'Single cell shaded'};

%% clean data
% irradiance/temperature dependence try 1
for n = [1:4]
    for i = 2:length(data(n).t)
        if data(n).v(i)/data(n).i(i) > 300 || data(n).v(i)/data(n).i(i) < 1
            data(n).v(i) = data(n).v(i-1);
            data(n).i(i) = data(n).i(i-1);
        end
    end
    data(n).v = movmean(data(n).v, 3);
    data(n).i = movmean(data(n).i, 5);
end
% irradiance/temperature dependence try 2
for n = [11:14]
    for i = 2:length(data(n).t)
        if data(n).v(i)/data(n).i(i) > 450 || data(n).v(i)/data(n).i(i) < 1
            data(n).v(i) = data(n).v(i-1);
            data(n).i(i) = data(n).i(i-1);
        end
    end
    data(n).v = movmean(data(n).v, 3);
    data(n).i = movmean(data(n).i, 5);
end
% irradiance/temperature dependence try 3
for n = [21:24]
    for i = 2:length(data(n).t)
        if data(n).v(i)/data(n).i(i) > 200 || data(n).v(i)/data(n).i(i) < 1
            data(n).v(i) = data(n).v(i-1);
            data(n).i(i) = data(n).i(i-1);
        end
    end
    data(n).v = movmean(data(n).v, 3);
    data(n).i = movmean(data(n).i, 7);
end


% series try 4
data(25).v = data(25).v + 0.1; %offset in voltage data
data(26).v = data(26).v + 0.1; %offset in voltage data
data(27).v = data(27).v + 0.13; %offset in voltage data

for n = [25:27]
    for i = 2:length(data(n).t)
        if data(n).v(i)/data(n).i(i) < 1 || data(n).i(i) < 0
            %data(n).v(i) = data(n).v(i-1);
            data(n).i(i) = data(n).i(i-1);
        end
        if data(n).v(i)/data(n).i(i) > 1050
            data(n).v(i) = data(n).v(i-1);
            data(n).i(i) = data(n).i(i-1);
        end
    end
    data(n).v = movmean(data(n).v, 3);
    data(n).i = movmean(data(n).i, 7);
end

% parallel try 3
for n = [18:20]
    for i = 2:length(data(n).t)
        if data(n).v(i)/data(n).i(i) > 205|| data(n).v(i)/data(n).i(i) < 1 || data(n).i(i) < 0
            data(n).v(i) = data(n).v(i-1);
            data(n).i(i) = data(n).i(i-1);
        end
    end
    data(n).v = movmean(data(n).v, 3);
    data(n).i = movmean(data(n).i, 5);
end

