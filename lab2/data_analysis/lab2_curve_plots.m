
open_data;
data_n = 25:27;

%plot all IU curves
figure
hold on
for i = data_n
    scatter(data(i).v,data(i).i * 1000)
end
hold off
title('IU Curves for 8s')
xlabel('U [V]') 
ylabel('I [mA]')
legend(shading_labels);

%plot all PU curves
figure
hold on
for i = data_n
    scatter(data(i).v, data(i).v.*data(i).i*1000)
end
hold off
title('PU Curves for 8s')
xlabel('U [V]') 
ylabel('P [mW]')
legend(shading_labels);

%plot all effciiency-voltage curves
figure
hold on
for i = data_n
    scatter(data(i).v, data(i).v.*data(i).i./data(i).in_P*100)
end
hold off
title('Efficiency Curves for 8s')
xlabel('U [V]') 
ylabel('\eta [%]')
legend(shading_labels);

data_n = 18:20;

%plot all IU curves
figure
hold on
for i = data_n
    scatter(data(i).v,data(i).i * 1000)
end
hold off
title('IU Curves for 3s2p')
xlabel('U [V]') 
ylabel('I [mA]')
legend(shading_labels);

%plot all PU curves
figure
hold on
for i = data_n
    scatter(data(i).v, data(i).v.*data(i).i*1000)
end
hold off
title('PU Curves for 3s2p')
xlabel('U [V]') 
ylabel('P [mW]')
legend(shading_labels);

%plot all effciiency-voltage curves
figure
hold on
for i = data_n
    scatter(data(i).v, data(i).v.*data(i).i./data(i).in_P*100)
end
hold off
title('Efficiency Curves for 3s2p')
xlabel('U [V]') 
ylabel('\eta [%]')
legend(shading_labels);
