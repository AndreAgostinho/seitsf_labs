clear
open_data;



e = 21;

sc_th = 0.3;
oc_th = 9e-3;

cell_temp = conditions.cellTemperature__C_(e);
Ut = 0.026*(273.15+cell_temp)/300;


%% determine Isc
sc_i = [];
j = 1;
for i = 1:length(data(e).t)
    if (data(e).v(i) < sc_th)
        sc_i(j) = data(e).i(i);
        j = j+1;
    end
end

Isc = mean(sc_i)

%% determine Voc
oc_v = [];
oc_i = [];
j = 1;
for i = 1:length(data(e).t)
    if (data(e).i(i) < oc_th)
        oc_v(j) = data(e).v(i);
        oc_i(j) = data(e).i(i);
        j = j+1;
    end
end

Voc = mean(oc_v)
Voc_i = mean(oc_i)

%% determine MPP

[max_p, mpp] = max(data(e).v.*data(e).i);
max_p
mpp_v = [];
mpp_i = [];
j = 1;
    
for i = 1:length(data(e).t)
    if (data(e).v(i) < data(e).v(mpp)+0.03 && data(e).v(i) > data(e).v(mpp)-0.03)
        mpp_v(j) = data(e).v(i);
        mpp_i(j) = data(e).i(i);
        j = j+1;
    end
end

max_p_v = mean(mpp_v)
max_p_i = mean(mpp_i)
max_eff = max_p_v*max_p_i/conditions.inputPower_W_(e)*100


%% Fit the data
% discount the photoelectric current from the current data
Id = Isc - data(e).i; % diode current

% get only exponential part for fitting
j = 1;
for i = 1:length(data(e).t)
    if data(e).v(i) > 1.1 && data(e).v(i) < 1.6
        v_fit(j) = data(e).v(i);
        i_fit(j) = Id(i);
        j=j+1;
    end
end

% try to find a straight line in a log log plot
fit_log = fit(v_fit',log(i_fit)','a*x+b', 'StartPoint',[log(2e-7) 8])
coeffs = coeffvalues(fit_log);

% % plot the fit
% figure
% title('Fitting y=ax+b to log(Id)')
% xlabel('U [V]') 
% ylabel('log(Id)')
% 
% hold on
% scatter(data(e).v,log(Id), 'd')
% plot(fit_log)
% legend({'log(Id)','fit y=ax+b'});
% equation = sprintf('y = %.3f * x %+.3f', coeffs(1), coeffs(2));
% text(0.6, -6, equation);
% hold off


Is_fit = exp(coeffs(2))
n_fit = 1/(coeffs(1)*3*Ut)

%% Fit two data points to model
n_teo = (max_p_v - Voc)/log((max_p_i-Isc)/(Voc_i-Isc))*(1/Ut/3)
Is_teo = (Isc-Voc_i)/(exp(Voc/(3*n_teo*Ut)))


%% Calculate the current using the equations with the parameters found
I_eq_fit = Isc - Is_fit * (exp(data(e).v/Ut/3/n_fit) - 1);
I_eq_teo = Isc - Is_teo * (exp(data(e).v/Ut/3/n_teo) - 1);

%% Calculate mean square errors
error_fit = mean((I_eq_fit-data(e).i).^2)
error_teo = mean((I_eq_teo-data(e).i).^2)

%% Run the model with fitted parameters

% %% init 3s model
% sim_time = 1;
% min_R = 0;
% max_R = 1000;
% slope_R = max_R/sim_time;
% meas_temp = conditions.cellTemperature__C_(e);
% cell = struct;
% 
% for i = 1:3
%     cell(i).Ipv = Isc;
%     cell(i).n = n_teo;
%     cell(i).Is = Is_teo;
%     cell(i).temp = conditions.cellTemperature__C_(e);%[ºC]
% end
% 
% out = sim('../model/exp_model_1M3P_3s');

%% plot everything
figure
hold on
scatter(data(e).v, data(e).i*1000, '.')
scatter(data(e).v, I_eq_fit*1000, '*')
scatter(data(e).v, I_eq_teo*1000, '+')
%plot(out.V_out.Data, out.I_out.Data);
hold off
title('Data vs model w/ fitted parameters experiment 1')
xlabel('U [V]') 
ylabel('I [mA]')
legend({'Data','All data fitting', 'Two point fitting'})