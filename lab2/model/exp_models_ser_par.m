close all

open_data;
%% init 8s model
sim_time = 1;
min_R = 0.1;
max_R = 3000;
slope_R = max_R/sim_time;
Ipv = 39.8e-3;
input_P = 2.08;%[W]
cell = struct;
meas_temp = 30;
data_n = 25:27;
for i = 1:8
    cell(i).Ipv = Ipv;
    cell(i).n = 1.08;
    cell(i).Is = 0.212e-9;
    cell(i).temp = 30;%[ºC]
end


%%% no shading

out1 = sim('exp_model_1M3P_8s');

%%% half cell shading

cell(1).Ipv = Ipv*0.5;

out2 = sim('exp_model_1M3P_8s');

%%% single cell shading

cell(1).Ipv = Ipv*0.01;
max_R = 100000
out3 = sim('exp_model_1M3P_8s');

%% plot results
% IU curves

figure
hold on
for i = data_n
    scatter(data(i).v,data(i).i * 1000, '.')
end
plot(out1.V_out.Data, out1.I_out.Data*1000);
plot(out2.V_out.Data, out2.I_out.Data*1000);
plot(out3.V_out.Data, out3.I_out.Data*1000);

title('Simulated vs experimental IU Curves for 8s')
xlabel('U [V]') 
ylabel('I [mA]')
legend([shading_labels 'No shading (sim)' 'Half cell shaded (sim)' 'Single cell shaded (sim)']);
hold off

% PU curves
figure
hold on
for i = data_n
    scatter(data(i).v, data(i).v.*data(i).i*1000, '.')
end
plot(out1.V_out.Data, out1.I_out.Data.*out1.V_out.Data*1000);
plot(out2.V_out.Data, out2.I_out.Data.*out2.V_out.Data*1000);
plot(out3.V_out.Data, out3.I_out.Data.*out3.V_out.Data*1000);

hold off

title('Simulated vs experimental PU Curves for 8s')
xlabel('U [V]') 
ylabel('P [mW]')
legend([shading_labels 'No shading (sim)' 'Half cell shaded (sim)' 'Single cell shaded (sim)']);

% efficiency curves
figure
hold on
for i = data_n
    scatter(data(i).v, data(i).v.*data(i).i./data(i).in_P*100, '.')
end
plot(out1.V_out.Data, out1.I_out.Data.*out1.V_out.Data/input_P*100);
plot(out2.V_out.Data, out2.I_out.Data.*out2.V_out.Data/input_P*(1-0.5/8)*100);
plot(out3.V_out.Data, out3.I_out.Data.*out3.V_out.Data/input_P*(1-1/8)*100);

title('Simulated vs experimental efficiency Curves for 8s')
xlabel('U [V]') 
ylabel('\eta [%]')
legend([shading_labels 'No shading (sim)' 'Half cell shaded (sim)' 'Single cell shaded (sim)']);

hold off


%% init 3s2p model
sim_time = 1;
min_R = 0;
max_R = 1000;
slope_R = max_R/sim_time;
Ipv = 53.8e-3;
input_P = 1.794;%[W]
meas_temp = 33;
cell = struct;
data_n = 18:20;
for i = 1:6
    cell(i).Ipv = Ipv;
    cell(i).n = 2.09;
    cell(i).Is = 4.3e-6;
    cell(i).temp = 33;%[ºC]
end

%%% no shading

out1 = sim('exp_model_1M3P_3s2p');

%%% half cell shading

cell(1).Ipv = Ipv/2;

out2 = sim('exp_model_1M3P_3s2p');

%%% single cell shading

cell(1).Ipv = 0;

out3 = sim('exp_model_1M3P_3s2p');


%% plot results
% IU curves
figure
hold on
for i = data_n
    scatter(data(i).v,data(i).i * 1000, '.')
end
plot(out1.V_out.Data, out1.I_out.Data*1000);
plot(out2.V_out.Data, out2.I_out.Data*1000);
plot(out3.V_out.Data, out3.I_out.Data*1000);

title('Simulated vs experimental IU Curves for 3s2p')
xlabel('U [V]') 
ylabel('I [mA]')
legend([shading_labels 'No shading (sim)' 'Half cell shaded (sim)' 'Single cell shaded (sim)']);
hold off

% PU curves
figure
hold on
for i = data_n
    scatter(data(i).v, data(i).v.*data(i).i*1000, '.')
end
plot(out1.V_out.Data, out1.I_out.Data.*out1.V_out.Data*1000);
plot(out2.V_out.Data, out2.I_out.Data.*out2.V_out.Data*1000);
plot(out3.V_out.Data, out3.I_out.Data.*out3.V_out.Data*1000);

hold off

title('Simulated vs experimental PU Curves for 3s2p')
xlabel('U [V]') 
ylabel('P [mW]')
legend([shading_labels 'No shading (sim)' 'Half cell shaded (sim)' 'Single cell shaded (sim)']);


% efficiency curves
figure
hold on
for i = data_n
    scatter(data(i).v, data(i).v.*data(i).i./data(i).in_P*100, '.')
end
plot(out1.V_out.Data, out1.I_out.Data.*out1.V_out.Data/input_P*100);
plot(out2.V_out.Data, out2.I_out.Data.*out2.V_out.Data/input_P*(1-0.5/8)*100);
plot(out3.V_out.Data, out3.I_out.Data.*out3.V_out.Data/input_P*(1-1/8)*100);

title('Simulated vs experimental efficiency Curves for 3s2p')
xlabel('U [V]') 
ylabel('\eta [%]')
legend([shading_labels 'No shading (sim)' 'Half cell shaded (sim)' 'Single cell shaded (sim)']);

hold off

