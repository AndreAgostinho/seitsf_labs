%% init 3s model
sim_time = 1;
min_R = 0;
max_R = 3000;
slope_R = max_R/sim_time;

n = 1.7;
Is = 1e-6;
meas_temp = 33;

cell = struct;



%%% 50 cm 33ºC
Isc = 0.02;
temp = conditions.cellTemperature__C_(21);
for i = 1:3
    cell(i).Ipv = Isc;
    cell(i).n = n;
    cell(i).Is = Is;
    cell(i).temp = temp;%[ºC]
end

out1 = sim('exp_model_1M3P_3s');

%%% 40 cm 33ºC
Isc = 0.04;
temp = conditions.cellTemperature__C_(22);
for i = 1:3
    cell(i).Ipv = Isc;
    cell(i).n = n;
    cell(i).Is = Is;
    cell(i).temp = temp;%[ºC]
end


out2 = sim('exp_model_1M3P_3s');

%%% 25 cm 30ºC
Isc = 0.1;
temp = conditions.cellTemperature__C_(23);
for i = 1:3
    cell(i).Ipv = Isc;
    cell(i).n = n;
    cell(i).Is = Is;
    cell(i).temp = temp;%[ºC]
end


out3 = sim('exp_model_1M3P_3s');

%%% 25 cm 45ºC
Isc = 0.1;
temp = conditions.cellTemperature__C_(24);
for i = 1:3
    cell(i).Ipv = Isc;
    cell(i).n = n;
    cell(i).Is = Is;
    cell(i).temp = temp;%[ºC]
end


out4 = sim('exp_model_1M3P_3s');

%% plot results
% IU curves
figure
hold on
plot(out1.V_out.Data, out1.I_out.Data);
plot(out2.V_out.Data, out2.I_out.Data);
plot(out3.V_out.Data, out3.I_out.Data);
plot(out4.V_out.Data, out4.I_out.Data);

hold off

% PU curves
figure
hold on
plot(out1.V_out.Data, out1.I_out.Data.*out1.V_out.Data);
plot(out2.V_out.Data, out2.I_out.Data.*out2.V_out.Data);
plot(out3.V_out.Data, out3.I_out.Data.*out3.V_out.Data);
plot(out4.V_out.Data, out4.I_out.Data.*out4.V_out.Data);

hold off

% efficiency curves
figure
hold on
input_P = conditions.inputPower_W_(21);
plot(out1.V_out.Data, out1.I_out.Data.*out1.V_out.Data/input_P);
input_P = conditions.inputPower_W_(22);
plot(out2.V_out.Data, out2.I_out.Data.*out2.V_out.Data/input_P);
input_P = conditions.inputPower_W_(23);
plot(out3.V_out.Data, out3.I_out.Data.*out3.V_out.Data/input_P);
plot(out4.V_out.Data, out4.I_out.Data.*out4.V_out.Data/input_P);

hold off



