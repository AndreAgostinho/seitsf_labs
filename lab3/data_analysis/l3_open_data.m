addpath('../data');

%% Put all file contents in a matrix
files = dir('../data/l3_*.csv');
conditions = readtable('../data/l3_experiment_conditions.csv');
k=1;
data = struct();

for i = 1:13
    s = sprintf('../data/l3_exp%02d*.csv',i);
    f = dir(s);
    for j = 1:length(f)
        a = fopen(f(j).name);
        if a ~= -1
            data_array = table2array(readtable(f(j).name));
            data(k).t = data_array(:, 1);
            data(k).v = data_array(:, 2);
            data(k).i = data_array(:, 4);
            k=k+1;
        end
    end
end




%% clean and correct data
data_to_clean = 1:15;
for n = data_to_clean
    data(n).v = data(n).v + 30e-3; %voltage data offset
    data(n).i = data(n).i + 4e-3; %current data offset

    
    data(n).v = movmean(data(n).v, 5);
    data(n).i = movmean(data(n).i, 10);
    
    for i = 2:length(data(n).t)
        if data(n).v(i)/data(n).i(i) > 1000
            data(n).v(i) = data(n).v(i-1);
            data(n).i(i) = data(n).i(i-1);
        end
    end
end
