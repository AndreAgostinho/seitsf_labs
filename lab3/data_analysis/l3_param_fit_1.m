clear
l3_open_data;

data_n = 1;

sc_th = 0.3;
oc_th = 6e-3;

cell_temp = conditions.cellTemperature__C_(data_n);
Ut = 0.026*(273.15+cell_temp)/300;


%% determine Isc
sc_i = [];
j = 1;
for i = 1:length(data(data_n).t)
    if (data(data_n).v(i) < sc_th)
        sc_i(j) = data(data_n).i(i);
        j = j+1;
    end
end

Isc = mean(sc_i);

%% determine Voc
oc_v = [];
oc_i = [];
j = 1;
for i = 1:length(data(data_n).t)
    if (data(data_n).i(i) < oc_th)
        oc_v(j) = data(data_n).v(i);
        oc_i(j) = data(data_n).i(i);
        j = j+1;
    end
end

Voc = mean(oc_v);
Voc_i = mean(oc_i);

%% determine MPP

[max_p, mpp] = max(data(data_n).v.*data(data_n).i);
mpp_v = [];
mpp_i = [];
j = 1;
    
for i = 1:length(data(data_n).t)
    if (data(data_n).v(i) < data(data_n).v(mpp)+0.03 && data(data_n).v(i) > data(data_n).v(mpp)-0.03)
        mpp_v(j) = data(data_n).v(i);
        mpp_i(j) = data(data_n).i(i);
        j = j+1;
    end
end

max_p_v = mean(mpp_v);
max_p_i = mean(mpp_i);
max_eff = max_p_v*max_p_i/conditions.inputPower_W_(data_n)*100;

%% Fit two data points to model
n_teo = (max_p_v - Voc)/log((max_p_i-Isc)/(Voc_i-Isc))*(1/Ut/conditions.seriesCells(data_n));
Is_teo = (Isc-Voc_i)/(exp(Voc/(conditions.seriesCells(data_n)*n_teo*Ut)));


%% Calculate the current using the equations with the parameters found
I_eq_teo = Isc - Is_teo * (exp(data(data_n).v/Ut/conditions.seriesCells(data_n)/n_teo) - 1);

%% Calculate mean square errors
error_teo = mean((I_eq_teo-data(data_n).i).^2);

%% plot everything
figure
hold on
scatter(data(data_n).v, data(data_n).i*1000, '.')
scatter(data(data_n).v, I_eq_teo*1000, '+')
hold off
title('Data vs model w/ fitted parameters experiment 1')
xlabel('U [V]') 
ylabel('I [mA]')
legend({'Data','Two point fitting'})

%% store parameters in table for simulation purposes
Experiment = {'experiment 1'};
Temperature = 30;
I_pv = Isc;
Is = Is_teo;
n = n_teo;
MSE = error_teo;
InputPower = conditions.inputPower_W_(data_n);
fit_cell_param = table(Experiment, InputPower, Temperature, I_pv, Is, n, MSE);
writetable(fit_cell_param, '../report/fit_cell_param.csv')