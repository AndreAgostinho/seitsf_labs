close all

l3_open_data;

% baseline
data_n = 1:3;
labels = {'exp1 - no BP diodes/no shading', ...
    'exp2 - no BP diodes/totally shaded', ...
    'exp3 - with BP diodes/no shading'};
graph_title = 'for baselines';

plot_cell_curves

% shading in 1st group comparison
data_n = [3 4:8];
labels = {'exp3 - no shading', 'exp4 - 100% + 100%', 'exp5 - 100% + 50%', ...
    'exp6.1 - 50% + 50%', 'exp6.2 - 100% + 0%', 'exp7 - 50% + 0%'};
graph_title = 'for 1st group shading';

plot_cell_curves

% shading in 2st group comparison
data_n = [3 9:13];
labels = {'exp3 - no shading', 'exp8 - 100% + 100%', 'exp9 - 100% + 50%', ...
    'exp10.1 - 50% + 50%', 'exp10.2 - 100% + 0%', 'exp11 - 50% + 0%'};
graph_title = 'for 2st group shading';

plot_cell_curves

% shading in both groups comparison
data_n = [3 14:15];
labels = {'exp3 - no shading', 'exp12 - 2x50% 1st group + 2x25% 2nd group',...
    'exp12 - 2x50% 1st group + 1x50% 2nd group'};
graph_title = 'for both group shading';

plot_cell_curves