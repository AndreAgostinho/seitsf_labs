clear
l3_open_data;

data_n = 1:15;

sc_th = 5; %ohm
oc_n = 50; %number of biggest points
oc_th = 400; %ohm

Isc = zeros(15,1);
Voc = zeros(15,1);
max_p = zeros(15,1);
max_p_v = zeros(15,1);
max_p_i = zeros(15,1);
max_eff = zeros(15,1);
Experiment = conditions.dataId;

for n = data_n
    %% determine Isc
    sc_i = [];
    j = 1;
    for i = 1:length(data(n).t)
        if (data(n).v(i)/data(n).i(i) < sc_th)
            sc_i(j) = data(n).i(i);
            j = j+1;
        end
    end

    Isc(n) = mean(sc_i);

    %% determine Voc
    [oc_hv,oc_index] = maxk(data(n).v, oc_n);
    oc_v = [];
    j = 1;
    for i = 1:length(oc_hv)
        if (oc_hv(i)/data(n).i(oc_index(i)) > oc_th)
            oc_v(j) = data(n).v(oc_index(i));
            j = j+1;
        end
    end

    Voc(n) = mean(oc_v);

    %% determine MPP

    [max_p(n), mpp] = max(data(n).v.*data(n).i);
    mpp_v = [];
    mpp_i = [];
    j = 1;

    for i = 1:length(data(n).t)
        if (data(n).v(i) < data(n).v(mpp)+0.03 && data(n).v(i) > data(n).v(mpp)-0.03)
            mpp_v(j) = data(n).v(i);
            mpp_i(j) = data(n).i(i);
            j = j+1;
        end
    end

    max_p_v(n) = mean(mpp_v);
    max_p_i(n) = mean(mpp_i);
    max_eff(n) = max_p_v(n)*max_p_i(n)/conditions.inputPower_W_(n)*100;

end

%% store parameters in table for comparison
Isc = Isc*1000;
max_p = max_p*1000;
max_p_i = max_p_i * 1000;

comp_table = table(Experiment, Voc, Isc, max_p, max_p_v, max_p_i, ...
    conditions.sh_cell0, conditions.sh_cell1, conditions.sh_cell2, conditions.sh_cell3);
comp_table.Properties.VariableNames{7} = 'Cell0_sh';
comp_table.Properties.VariableNames{8} = 'Cell1_sh';
comp_table.Properties.VariableNames{9} = 'Cell2_sh';
comp_table.Properties.VariableNames{10} = 'Cell3_sh';
writetable(comp_table, '../report/comp_table.csv')