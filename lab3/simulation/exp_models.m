close all

addpath('../data_analysis/');
l3_open_data;
params = readtable('../report/fit_cell_param.csv');
%% init 7s model with bypass diodes using fitted parameters
sim_time = 1;%s
min_R = 0.1;%ohm
max_R = 10000;%ohm
slope_R = max_R/sim_time;
Ipv = params.I_pv(1);
cell = struct;
meas_temp = params.Temperature(1);

for i = 1:7
    cell(i).Ipv = params.I_pv;
    cell(i).n = params.n;
    cell(i).Is = params.Is;
    cell(i).temp = params.Temperature;%[ºC]
end

%bypass diodes
turned_off = 0;
turned_on = 1;
BD1 = turned_off;
BD2 = turned_off;


%% perform simulations with different shadings
data_n = 1:15;
for n = data_n
    % put the correct shading
    cell(1).Ipv = (1-conditions.sh_cell1(n)) * params.I_pv;
    cell(2).Ipv = (1-conditions.sh_cell2(n)) * params.I_pv;
    cell(3).Ipv = (1-conditions.sh_cell3(n)) * params.I_pv;
    cell(4).Ipv = (1-conditions.sh_cell4(n)) * params.I_pv;
    % put the correct diode state
    BD1 = conditions.BD(n);
    BD2 = conditions.BD(n);
    out(n) = sim('exp_model_1M3P_7s_BP');
end

%% plot results

% baseline
data_n = 1:3;
labels = {'exp1 - no BP diodes/no shading', ...
    'exp2 - no BP diodes/totally shaded', ...
    'exp3 - with BP diodes/no shading'};
graph_title = 'for simulated baselines';

plot_sim_curves

% shading in 1st group comparison
data_n = [3 4:8];
labels = {'exp3 - no shading', 'exp4 - 100% + 100%', 'exp5 - 100% + 50%', ...
    'exp6.1 - 50% + 50%', 'exp6.2 - 100% + 0%', 'exp7 - 50% + 0%'};
graph_title = 'for simulated 1st group shading';

plot_sim_curves

% shading in 2st group comparison
data_n = [3 9:13];
labels = {'exp3 - no shading', 'exp8 - 100% + 100%', 'exp9 - 100% + 50%', ...
    'exp10.1 - 50% + 50%', 'exp10.2 - 100% + 0%', 'exp11 - 50% + 0%'};
graph_title = 'for simulated 2st group shading';

plot_sim_curves

% shading in both groups comparison
data_n = [3 14:15];
labels = {'exp3 - no shading', 'exp12 - 2x50% 1st group + 2x25% 2nd group',...
    'exp12 - 2x50% 1st group + 1x50% 2nd group'};
graph_title = 'for simulated both group shading';

plot_sim_curves