%plot IU curves
figure
hold on
grid on
for i = data_n
    scatter(out(i).V_out.Data,out(i).I_out.Data * 1000, '.')
end
hold off
title(['IU Curves ' graph_title])
xlabel('U [V]') 
ylabel('I [mA]')
legend(labels)

%plot all PU curves
figure
hold on
grid on
for i = data_n
    scatter(out(i).V_out.Data, out(i).V_out.Data.*out(i).I_out.Data*1000, '.')
end
hold off
title(['PU Curves ' graph_title])
xlabel('U [V]') 
ylabel('P [mW]')
legend(labels)

%plot all effciency-voltage curves
figure
hold on
grid on
for i = data_n
    scatter(out(i).V_out.Data, out(i).V_out.Data.*out(i).I_out.Data./conditions.inputPower_W_(i)*100, '.')
end
hold off
title(['Efficiency Curves ' graph_title])
xlabel('U [V]') 
ylabel('\eta [%]')
legend(labels)
